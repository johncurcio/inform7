"Escape" by Filipe Ferreira, Guido Vincent, João Curcio

Section 1 - Introduction

The story genre is "Emotional Puzzler". The release number is 1. The story headline is "Escape". The story description is 
"You wake up in a room alone with no memory. Travel through the objects in the room to find out who you are and where you are.

". The story creation year is 2017. Release along with an interpreter.

Part 1 - Adjustments of the World

Section 1 - Inventory

Instead of searching the player, try taking inventory.

Section 2 - Containers

Rule for printing the name of the coat: 
	say "[printed name of the coat]";
	omit contents in listing.

Rule for printing the name of the box of jewelry: 
	say "[printed name of the box of jewelry]";
	omit contents in listing.

Part 2 - Hidden Memories

Section 1 - Memories

Table of Memory
item	memory
"coat"	"[italic type]It used to belong to someone you knew. But who?[roman type]"
"silver key"	"[italic type]This key. You remember it. It's used to unlock your box of jewelry.[roman type]"
"La Mort de Marat"	"You see a small calendar page glued there. [bold type]It was April 4th when you left home the last time. Such a tragic day to disappear. Shi is an eerie number for a Japanese like you.[roman type]"
"Girl Before a Mirror"	"Mothballs and cockroaches flee when you move the paiting. You're relieved to not be alone. [bold type]For the second time since your twin sister died you're not alone anymore.[roman type]"
"Las Meninas"	"Red splotches cover the back. It reminds you of how [bold type]you polished the nails of a single hand in morbid velvet. You wonder if it's a lucky number or if you didn't have the time to finish your nails.[roman type]"
"Lucky Night"	"The numbers 1,2,3,4,5,6 are written in the back. [bold type]'There are six sides to luck as in a die: ichi, ni, san, shi, go, roku'; your father used to joke. He was always particularly bad with jokes... alas!, fathers.[roman type]"
"The Elephants"	"You thought there was a note glued to the back. But it was just a fleeting ilusion. [italic type]Your twin sister. She left without you. For the first time in 20 years you were not together anymore.[roman type]"
"The Man Withe Compass"	"A spider hides herself when you look. [italic type]You wanted to get lost when you left home. It seems like you've achieved it. You are lost![roman type]"
"box of jewelry"	"[italic type]It shines like your personality used to. Perhaps being trapped in an unknow location is draining you down.[roman type]"
"pocket-note"	"[italic type]You used to pass notes all the time to your sisters in school. You miss them.[roman type]"
"attic key"	"[italic type]You and your sister used to hide in the attic when you were youngers. You would lock yourselves in there and only leave when you were hungry.[roman type]"
"silver box"	"It looks valuable."
"chopsticks"	"[italic type]You never liked chopsticks. Not even when you were little. They're too cumbersome to eat with. You prefer forks.[roman type]"
"glass box"	"Taken."
"glass key"	"Taken."
"flashlight"	"Taken."

After taking the noun:
	choose row with a item of "[printed name of the noun]" in The Table of Memory;
	say "[memory entry][paragraph break]".
	
Section 2 - Notes

A note is a kind of thing. A note has some text called printing. The printing of a note is usually "blank".

Understand the command "read" as something new. Reading is an action applying to one thing. Understand "read [something]" as reading. Check reading: if the printing of the noun is "blank", say "Nothing is written on [the noun]." instead. Carry out reading: say "You read: [printing of the noun][line break]". Report reading: do nothing.

Section 3 - Clues

A display is a kind of thing. A display is always scenery. Understand "look behind [something]" as looking under.

Instead of looking under a display: 
	say " ".

Before looking under the display:
	choose row with a item of "[printed name of the noun]" in The Table of Memory;
	say "[memory entry][paragraph break]".

Instead of taking the display, say "Thief! Put that back! [paragraph break]You put the display back in place.".

Instead of examining south, say "You see Lucky Night, by Leonid Afremov.".
Instead of examining north, say "You see La Mort de Marat, by Jacques-Louis David.".
Instead of examining southeast, say "You see Las Meninas, by Diego Velázquez.".
Instead of examining southwest, say "You see The Man Withe Compass, by Hans Holbein Le Jeune.".
Instead of examining northeast, say "You see Girl Before a Mirror, by Pablo Picasso.".
Instead of examining northwest, say "You see The Elephants, by Salvador Dalí.".

A person has a number called visto. The visto of the player is 0. Before looking under la mort de marat: increase the visto of the player by 1. 

Chapter 1 - The Hexagon Chamber

When play begins: say "[story description]".

Hexagon Chamber is a room. "A perfect hexagon-shaped room. With six different portraits in each wall. [Lucky Night] is at the south wall. [Man Withe Compass] is at the southwest wall. [Elephants] is at the northwest wall. [La Mort de Marat] is at the north wall. [Girl Before a Mirror] is at the northeast wall. [Las Meninas] is at the southeast wall.". 

The player is a person in the Hexagon Chamber. 

A floor is a kind of thing. Before putting something on a floor: try dropping the noun instead. A floor is always scenery. Instead of looking under a floor: say "Nice idea if you can figure out how." Understand "floor" or "ground" as a floor. Hexagon Chamber contains a floor.

A jacket is a kind of thing. A jacket is always wearable. The printed name of the jacket is "jacket". 
A pocket is a kind of container. A pocket is part of every jacket. The carrying capacity of a pocket is always 2.
After examining a jacket: 
	let target be a random pocket which is part of the noun; 
	say "[The target] contains [a list of things in the target]."

The coat is a jacket. The printed name of the coat is "coat". The description is "[italic type]You used to always be happy when you found gum and coins in your pockets. They don't appeal to you as much in this blood-drenched leather coat[roman type].". The coat is in the Hexagon Chamber. Understand "search [the coat]" as examining.

The pocket-note is a note. The coat's pocket contain the pocket-note. The printing of pocket-note is "[italic type]Whenever you are painting, the colors always seem to talk to you.[roman type][paragraph break] It makes you miss the texture of your canvas, the dryness of your brushes, the mess in your pallette, and more than anything the earthy smell of ink.". 

After wearing the coat:
	say "It weirdly fits you well.".

The box of jewelry is a locked container in the Hexagon Chamber. The printed name of the wooden box is "box of jewelry". The description is "A wooden box carved with initials AO. It makes you feel like this strange place is your bedroom. Where are you anyway?". The silver key unlocks the box of jewelry. The coat's pocket contain the silver key. 

Before unlocking the box of jewelry with the silver key: 
	if the silver key is not carried, say "You don't seem to have the right key to unlock this box." instead. 

After unlocking the box of jewelry with the silver key: 
	choose row with a item of "box of jewelry" in The Table of Memory;
	say "Ka-zan! It unlocks. You can't wait to see what's inside. [paragraph break]";
	try opening box of jewelry.

After opening the box of jewelry, say "Just great! A [bold type]silver box[roman type]. A box inside another box. How undeniably clever.".

[DISPLAYS]

La Mort de Marat is a display in the Hexagon Chamber. The description is "Given that I am unhappy, I have a right to your help. [italic type]You see a strange crack behind it.[roman type]" 

Girl Before a Mirror is a display in the Hexagon Chamber. The description is "The woman looking herself in the mirror looks like you. But her reflection looks like someone else.".

Las Meninas is a display in the Hexagon Chamber. The description is "Five girls stand there lifeless and bored. It reminds you of you and your sisters.".

Lucky Night is a display in the Hexagon Chamber. The description is "It's colorful, but deeply unsettling. How can such a sad picture be painted in such happy colors?".

Elephants is a display in the Hexagon Chamber. The printed name of the elephants is "The Elephants". The description is "Creepy and disturbing. Elephants shouldn't be that tall and skinny.".

Man Withe Compass is a display in the Hexagon Chamber. The printed name of the man withe compass is "The Man Withe Compass".  The description is "A compass always points north first and then directs you clockwise to where you want to go.".

[END DISPLAYS]

The silver box is in the box of jewelry. Instead of opening the silver box, say "The silver box opens only when [bold type]spinned[roman type] to the correct 4-number combination. You wonder where is this combination.".  In the silver box is the attic key. The silver box is closed. 

Spinning it to is an action applying to one thing and one number. Check spinning it to: if the noun is not the silver box, say "[The noun] does not spin." instead. Report spinning it to: say "Shini-goro! and nothing else happens." 

Understand "spin [something] to [a number]" as spinning it to. 

After spinning the closed silver box to 4256: 
	now the silver box is open; 
	say "Shini-goro! and the silver box door swings slowly open, revealing [a list of things in the silver box].".

After taking attic key:
	say "You feel relived. Maybe that leads me to the way out of here.".


Part 3 - Truths

Section 1 - Doors

The description of a door is usually "[if open]It stands open[otherwise]It is closed[end if][if locked] and locked. You wonder where's the key[otherwise] and unlocked[end if]. "

Chapter 2 - Cylinder Chamber

The Cylinder Chamber is a room above the Hexagon Chamber. The description is "A perfect cylindrical room, as the name suggests. [if the Cylinder Chamber is not visited]You're disappointed to see it's not the exit. How did you end up here?[end if]".

The wooden door is a locked door. The wooden door is outside from the Cylinder Chamber and inside from the Hexagon Chamber. The attic key unlocks the wooden door.

The chair is an open enterable container, fixed in place. The chair is in the cylinder chamber. Understand "sit on [chair]" as entering. Instead of entering chair, say "It feels comfortable and warm. How long has it been since you felt this nice? A day? A week? How long have you been here?". 

Rule for printing the name of the chair: 
	say "[printed name of the chair]";
	omit contents in listing.

The newspaper is a note, fixed in place. The description is "Try reading it". The printing of the newspaper is "The New Yorker. October 20th. [if the visto of the player is 1][bold type]Tears start pouring through your eyes. 'This can't be real' - you said to yourself. 'It's been 6 months that I'm gone? I need to get out of here. They must be looking for me!'[roman type][otherwise] Apparently someone died tragically a few months ago. You don't care about tragedies.[end if]". Understand "news" as newspaper.

The table is a supporter. The description is "Just a wooden table like the ones you have at home.". The table is in the cylinder chamber. The newspaper is on the table.

Rule for printing the name of the table: 
	say "[printed name of the table]";
	omit contents in listing.

The dinner is a thing that is edible. The dinner is on the table. The dinner is fixed in place. After eating the dinner, say "When was the last time you ate something? You are so hungry you don't even bother asking what's this doing here.". After examining the dinner, say "It's homemade katsudon! [italic type]You mom was the best cook in the whole world. Her homemade katsudon was divine and beyond comparison.[roman type]".

The chopsticks is a thing on the table. The description is "[italic type]One cannot be complete without the other. Like you and your sister.[roman type]".
Before eating the dinner:
	if the chopsticks is not carried, say "You can't have dinner without chopsticks." instead.

The rectangular mirror is a thing, fixed in place.  The rectangular mirror is in the cylinder chamber. The description is "A mirror that goes from top to bottom of the cylinder chamber.". After examining the rectangular mirror, say "You can see your reflection perfectly, except for a few finger marks on the sides. [italic type]You look so much like your sister even you couldn't tell who's who.[roman type]" instead. 

The pill of memory is a thing that is edible. The description is "A strong pill with a powerful effect. You can only eat one pill without passing out.".

After touching the rectangular mirror:
	now the player carries the pill of memory;
	say "The [bold type]pill of memory[roman type] falls into your hands. It can bring you back memories of the past.".

The oval mirror is a thing, fixed in place. The oval mirror is in the cylinder chamber. The description is "A mirror that makes you look fat.". After examining the rectangular mirror, say "You are upset. [italic type]You have always been the ugly duck in your family. And the sad one. Accessorizing with a leather coat never made you more likeable. Nobody liked you but your paintings and your sister. Wait, did she like you?[roman type]".

The pill of thought is a thing that is edible. The description is "A strong pill with a powerful effect. You can only eat one pill without passing out.".

After touching the oval mirror:
	now the player carries the pill of thought;
	say "The [bold type]pill of thought[roman type] falls into your hands. It can bring you enlightment in the workings of the room.".

The weird mirror is a thing, fixed in place. The weird mirror is in the cylinder chamber. The description is "A mirror that makes you look just the way you are: mishapen.". After examining the rectangular mirror, say "You feel unconsolable. [italic type]Your only friends have been the jewelry and the paitings for a decade. You've always been alone. If it weren't for your sister, you would be alone. But then she left as well. Your worst enemies have been the mirrors. They remind you that your sister is always with you. Even in death.[roman type]".

The pill of poison is a thing that is edible. The description is "Your way out. Isn't that what you were looking for?".

After touching the weird mirror:
	now the player carries the pill of poison;
	say "The [bold type]pill of poison[roman type] falls into your hands. It can bring you freedom and death. You've been fond of the easy way out anyway.".

After eating the pill of memory:
	say "It tasted refreshing: You finally remembered. [bold type]YOU are dead. [italic type]You are the sister. In April 4th, you killed yourself. You couldn't take it anymore. The loneliness, the fatigue, the depression. If only you had a friend. If only you could talk to someone.[roman type] You burst into tears uncontrollably. You're not lost. You're not anywere. This is the afterlife.";
	remove the pill of poison from play;
	remove the pill of thought from play;
	move the player to the end hallway;
	rule succeeds.

After eating the pill of poison:
	remove the pill of memory from play;
	remove the pill of thought from play;
	end the story saying "BAD ENDING: You wait a feel moments and finally the poison kicks in. It feels oddly familiar. [paragraph break]You couldn't figure out who you were, but you sure did escape. You hope your family is safe wherever they are. And you hope to see your sister in the afterlife.";
	rule succeeds.

The glass box is a locked container in the cylinder chamber. The description is "[italic type]Your sister gave this to you when you turned 20 years old. It said 'I love you'.[roman type]". The glass key is inside the glass box. The description is "It unlocks the great glass door.". The glass key unlocks the great glass door. The flashlight is a thing in the glass box. "A dark light to guide you through the next installment." The flashlight is lit.

After eating the pill of thought:
	say "It tasted disgusting! Maybe you should look around again.";
	now the glass box is unlocked;
	remove the pill of poison from play;
	remove the pill of memory from play;
	rule succeeds.

After opening the glass box:
	now the player carries the flashlight;
	now the player carries the glass key;
	say "[italic type]When you open the glass box, you start to remember things. How much your family loved you. How much your friends cared about you. And what happened before you wound up here: you opened up your mother's bathroom cabinet, you took the case of pills, and you ended your life.[roman type][paragraph break]You grab the contents of the glass box: a flashlight and a glass key.".

After unlocking the great glass door with the glass key: 
	say "Ka-bam! It unlocks. You feel like your journey is coming to an end. [paragraph break]";
	try opening the great glass door.

After entering the great glass door:
	now the great glass door is locked;
	remove the chopsticks from play;
	remove the attic key from play;
	remove the silver key from play;
	say "You've reached your final destination.".

Chapter 3 - The End Hallway

The End Hallway is a room above the Cylinder Chamber.  The description is "You can't see much. If only you had a flashlight.". 

The great glass door is a locked door. The great glass door is outside from the end hallway and inside from the cylinder chamber.

The gold door is a closed door. The gold door is inside from the end hallway and outside from nothingness. The description is "End of the road.".

After entering the gold door:
	end the story saying "OK ENDING: You have finally escape without quitting. You vanish in the realm of nothingness to never be found. While you're vanishing you remember one last thing: you forgot to tell your family you loved'em.".

The heaven door is a locked door. The heaven door can be found or lost. The heaven door is lost. The heaven door is east of the end hallway and west from paradise. The description is "Welcome to heaven.".

Rule for printing the name of the heaven door: 
	if the heaven door is lost, say "shadow of something";
	otherwise say "heaven door".

Understand "shadow of something" or "shadow" as heaven door.

Before opening the heaven door:
	if the heaven door is lost,
			say "You're afraid to touch what you can't see. If only you had a flashlight to examine the shadow." instead.
	
After entering the heaven door:
	if the player is wearing the coat,
		end the story saying "ALTERNATE ENDING: Your familiy is reunited here to receive you. They hug you and bid you goodbye, assuring you how much they've loved you. Your sister removes her coat and trades your blood-drenched coat with hers. You always liked to exchange coats with her. She hugs you strongly than ever and says: 'You are becoming a famous painter out here. You always wanted to be remembered. Now the world will know who you are.'. You ascend to Paradise completely fulfilled. Life was worth it.";
	otherwise end the story saying "GOOD ENDING: Your familiy is reunited here to receive you. They hug you and bid you goodbye, assuring you how much they've loved you. You ascend to Paradise happier than you've ever been.".

Visibility rule when examining the heaven door in the End hallway: 
	if the player is carrying a lit thing (called lamp): 
		now the heaven door is found;
		now the player carries the heaven key;
		say "You shine light on shadow of something. Reaveling the heaven door. A heaven key falls into your pocket."; 
		there is sufficient light; 
	there is insufficient light.

The heaven key unlocks the heaven door.

After unlocking the heaven door with the heaven key: 
	say "You feel a gush of wind in your face. It's the outside world.";
	try opening the heaven door.



